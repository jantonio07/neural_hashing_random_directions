Para instrucciones de como compilar vease CMakeLists.txt.

El binario resultante recibe 4 argumentos:

1 Archivo que contiene los vectores para hacer knn. Cada
linea es un vector y cada entrada esta separada por un espacio.

2 Archivo que contien las etiquetas del archivo que se pasa co-
mo primer argumento.
 
3 Numero maximo de elementos a regresar, es el k en KNN. 

4 Nombre del archivo de texto donde se almacenaran los resultados
