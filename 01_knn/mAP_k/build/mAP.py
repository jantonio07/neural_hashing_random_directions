import numpy as np
from debug_functions import average_precision

f = open('abc.txt')
n = 0

aP = []

for line in f:

	n += 1
	line = list(map(float, line.split()))
	ans = average_precision(line)
	aP.append(ans)

print(n)
print(np.mean(aP))