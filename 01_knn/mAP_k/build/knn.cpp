#include <bits/stdc++.h>
using namespace std;

typedef pair<double, int> di;

double d(vector<double> &a, vector<double> &b) {

	double ans = 0;

	for(int i=0; i<a.size(); i++)
		ans += (a[i]-b[i])*(a[i]-b[i]);

	return ans;

}


int main(int argn, char **argv) {

	string line;
	int n = 0;
	double xi;

	ifstream data(argv[1]);
	ifstream label(argv[2]);

	vector<vector<double> > x;
	vector<int> y;

	while(getline(data, line)) {

		stringstream ss(line);
		vector<double> nx;

		while(ss >> xi)
			nx.push_back(xi);

		x.push_back(nx);
		n++;

	}

	y.assign(n, 0);

	for(int i=0; i<n; i++)
		label >> y[i];

	/*
	for(int i=0; i<5; i++) {

		cout << x[i][0] << ' ' << x[i][1] << '\n';
		cout << y[i] << '\n';

	}
	*/

	int k = 249;
	double mAP = 0;

	for(int i=0; i<n; i++) {

		double aP = 0;
		int in_queue = 0;
		priority_queue<di> knn;

		for(int j=0; j<n; j++) {

			if(i == j)
				continue;

			double dis = d(x[i], x[j]);
			knn.push(di(dis, j));
			in_queue++;

			if(in_queue > k) {

				in_queue--;
				knn.pop();

			}

		}

		vector<di> distancias(k);
		vector<int> knn_etiquetas(k, -1);
		int j = k-1;

		while(!knn.empty()) {

			knn_etiquetas[j] = y[knn.top().second];
			distancias[j].first = knn.top().first;
			distancias[j].second = knn.top().second;

			knn.pop();

			knn_etiquetas[j] = knn_etiquetas[j] == y[i];

			j--;

		}

		int debug = 810;

		/*
		if(i == debug) {

                        for(int j=0; j<k; j++)
                                cout << distancias[j].first << ' ' << distancias[j].second << '\n';

                        cout << '\n';
                }
		*/

		int ok = 0;

		for(int j=0; j<k; j++)
			if(knn_etiquetas[j]) {

				ok++;
				aP += (double)ok/(j+1);

			}

		aP /= ok;
		mAP += aP;

		if(i == debug || 1) {

			cout << knn_etiquetas[0];

			for(int j=1; j<k; j++)
				cout << ' ' << knn_etiquetas[j];

			cout << '\n';

			//cout << aP << "\n\n";

		}

	}

	//cout << "Datos: " << n << '\n';
	//cout << "mAP: " << mAP/n << '\n';

	return 0;

}
