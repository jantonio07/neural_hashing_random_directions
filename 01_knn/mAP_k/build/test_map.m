n = 1250

B = load('experimento0/datos');
label = load('experimento0/labels');

a = 1

mat_d = zeros(1250, 1250);

a = 2

for i = 1:n
  for j = 1:n
    mat_d(i, j) = norm( B(i, :) - B(j, :), 2);
  end
 end

 a = 3
 
[~, ~, map] = compute_map(mat_d, label, label)