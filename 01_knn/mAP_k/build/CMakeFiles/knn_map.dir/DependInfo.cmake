# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/monroy/Documents/knn/mAP_k/knn_args.cpp" "/home/monroy/Documents/knn/mAP_k/build/CMakeFiles/knn_map.dir/knn_args.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/monroy/Documents/knn/dlib/.."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/monroy/Documents/knn/mAP_k/build/dlib_build/CMakeFiles/dlib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
