#include <iostream>
#include <dlib/statistics.h>
#include <dlib/matrix.h>
#include <time.h>

using namespace std;
using namespace dlib;


#define pb push_back


typedef pair<double, int> di;
typedef pair<int, int> ii;
typedef long long ll;


int string2int(string);
double string2double(string);
double distanciaAngulo(matrix<double> &, matrix<double> &, int, int, int);
double euclidiana(matrix<double> &, matrix<double> &, int, int, int);
std::vector<di> knn(matrix<double> &, matrix<double> &, int, int, int);


struct cca_outputs {

    matrix<double, 0, 1> correlations;
    matrix<double> Ltrans;
    matrix<double> Rtrans;

};


/*
	1 - Direccion del archivo donde estan los datos
	2 - Direccion del archivo que contiene las etiquetas
	3 - Numero de vecinos a buscar
	4 - Nombre del archivo donde se guardaran resultados

*/
int main(int narg, char **argv) {

	matrix<double> A;
	matrix<double> B;
	std::vector<int> labelA, labelB;
	string param, val;
	int nComp = -1;
	int KFirst;
	long int cotap = -1;
	bool mismoC = true;
	bool imprimirProgreso = false;
	string c1, c2;
	double recallSum = 0;
	bool imprimirError = false;

	ifstream inD(argv[1]);
	inD >> A;

	inD.close();

	ifstream inL(argv[2]);

	while(inL >> val)
		labelA.push_back(string2int(val));

	inL.close();

	if(mismoC) {

		B = A;
		labelB = labelA;

	}

	KFirst = string2int(argv[3]);

	if(nComp < 0)
		nComp = A.nc();

	if(cotap < 0)
		cotap = A.nr();

	int ok = 0, wrong = 0;
	int bienClasificados = 0;
	double mean_ap = 0;
	double ap;

	for(int i=0; i<min(A.nr(), cotap); i++) {

		if(imprimirProgreso)
			cout << i+1 << " de " << A.nr() << '\n';

		std::vector<di> distancias = knn(A, B, i, nComp, KFirst+mismoC);
		int inK = 0;
		map<int, int> reg;
		map<int, int>::iterator It;

		ap = 0;

		for(int j=0; j<distancias.size(); j++) {

			reg[labelB[distancias[j].second]]++;
			if(labelA[i] == labelB[distancias[j].second]) {

				inK++;
				ap += (double)inK/(j+1);

			}

		}

		if(inK != 0)
			ap /= inK;

		mean_ap += ap;

	}

	ofstream resultados(argv[4]);

	cout << "Datos: " << A.nr() << '\n';
	cout << "mAP: " << mean_ap/A.nr() << '\n';
	resultados << mean_ap/A.nr() << '\n';

	return 0;

}


int string2int(string str) {

	stringstream ss(str);
	int ans;

	ss >> ans;

	return ans;

}

double string2double(string str) {

	stringstream ss(str);
	double ans;

	ss >> ans;

	return ans;

}


double euclidiana(matrix<double> &A, matrix<double> &B, int a, int b, int k) {

	double d = 0;

	k = min((long)k, A.nc());

	for(int i=0;i<k; i++)
		d += (A(a, i)-B(b, i))*(A(a, i)-B(b, i));

	return d;

}


std::vector<di> knn(matrix<double> &A, matrix<double> &B, int a, int nc, int k) {

	double d;
	int ps = 0;
	std::priority_queue<di> distancias;

	for(int i=0; i<B.nr(); i++) {
		d = euclidiana(A, B, a, i, nc);

		//if(d > 2)
			//continue;

		ps++;
		distancias.push(di(d, i));

		if(ps > k) {

			distancias.pop();
			ps--;

		}

	}

	std::vector<di> ans(ps);
	ps--;

	while(!distancias.empty()) {

		ans[ps--] = distancias.top();
		distancias.pop();

	}

	return ans;

}
