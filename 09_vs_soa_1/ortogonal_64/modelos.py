import tensorflow as tf
import numpy as np
import sys
from sklearn.preprocessing import OneHotEncoder
from math import pi

#############################################################################################################

class Hashing_Base:

	'''
		X - Conjunto de datos para entrenar (cada fila es un ejemplar)
		Y - Clases correspondientes al conjunto de entrenamiento X
		epochs - Número de veces que pasa cada dato en el entrenamiento
		batch_size - Número de elementos que se toman para estimar el gradiente
		hot_encoding - True si las etiquetas se tienen que pasar de enteros a vectores
		k_ultimos - Número de elementos que se toman para ver si hay convergencia
		tolerancia - Referencia para el criterio de convergencia
	'''
	def entrenar(self, X, Y, epochs, batch_size, hot_encoding = True, k_ultimos = 10, tolerancia = 1e-3, carpeta = ""):

		if hot_encoding:
			#Y = tf.keras.utils.to_categorical(Y, num_classes = self.numero_clases)

			Y = np.array(Y)
			Y = Y.reshape(len(Y), 1)
			onehot_encoder = OneHotEncoder(sparse=False)
			Y = onehot_encoder.fit_transform(Y)

		errores_pasados = []

		##########################################################################

		valor_cp = -1
		saver = tf.train.Saver()
		carpeta_modelo = carpeta + "model_{0}.ckpt".format(self.metodo)

		for epoch in range(epochs):

			print("Epoch", epoch+1, "de", epochs, end = ' ({0}).'.format(self.metodo))
			error_sum = 0

			t_epochs = len(X)//batch_size

			if len(X)%batch_size != 0:
				t_epochs += 1

			for i_batch in range(t_epochs):
				start_ind = i_batch*batch_size
				end_ind = min(len(X), (i_batch+1)*batch_size)

				x_epoch = X[start_ind:end_ind]
				y_epoch = Y[start_ind:end_ind]

				feed_dict = {self.x_ph : x_epoch,
                			 self.y_ph : y_epoch}

				_, loss_val = self.sess.run([self.optimizer, self.cost], feed_dict = feed_dict)
				error_sum += loss_val

			print(" Loss function:", error_sum, end = '. ')

			##########################################################################

			errores_pasados.append(error_sum)

			if len(errores_pasados) == k_ultimos + 1:
				errores_pasados = errores_pasados[1:]

			print('std =', np.std(errores_pasados))

			if (len(errores_pasados) == k_ultimos) and (np.std(errores_pasados) < tolerancia):
				print('Criterio de convergencia cumplido')
				return

			if valor_cp == -1 or error_sum < valor_cp:

				valor_cp = error_sum
				print("Intentando guardar en", carpeta_modelo)
				save_path = saver.save(self.sess, carpeta_modelo)
				print("Modelo guardado en: {0}".format(save_path))

				registro = open(carpeta + 'funcion_error', 'w')
				registro.write('{0}\n'.format(error_sum))
				registro.close()

			##########################################################################

		registro = open(carpeta + 'funcion_error')
		for line in registro:
			error_guardado = float(line)

		if error_sum < error_guardado:
			print("Intentando guardar en", carpeta_modelo)
			save_path = saver.save(self.sess, carpeta_modelo)
			print("Modelo guardado en: {0}".format(save_path))

			registro = open(carpeta + 'funcion_error', 'w')
			registro.write('{0}\n'.format(error_sum))
			registro.close()
		else:
			print("cargando modelo")
			saver.restore(self.sess, carpeta_modelo)

		##########################################################################


	def agregar_optimizador(self):

		self.optimizer = tf.train.AdamOptimizer().minimize(self.cost)


	'''
		X - Conjunto de datos a los que se quiere aplicar transformación (penúltima capa del modelo neuronal)
	'''
	def transformacion(self, X):

		X = np.atleast_2d(X)

		feed_dict = {self.x_ph : X}
		return self.sess.run(self.transformation, feed_dict = feed_dict)


	def aplicar_hashing(self, X):

		X = np.atleast_2d(X)

		feed_dict = {self.x_ph : X}
		return self.sess.run(self.hash, feed_dict = feed_dict)


	def pesos_hash(self):

		return self.sess.run(self.weight_hash)


	def clasificacion_transformacion(self, X):

		x_place = tf.placeholder(np.float32, shape = (None, self.nodos_capa[-1]))

		class_trans = tf.nn.softmax(tf.matmul(x_place, self.weight[-1]) + self.bias[-1])

		X = np.atleast_2d(X)

		feed_dict = {x_place : X}
		return self.sess.run(class_trans, feed_dict = feed_dict)


#############################################################################################################


#############################################################################################################

class hashing_hiperplanos(Hashing_Base):

	'''
		d -Dimensión de los datos
		nodos_capa - Lista con nodos, nodos_capa[i] es la cantidad de nodos que tiene la i-ésima capa
		numero_clases - Número de clases en el problema de clasificación
	'''
	def __init__(self, d, nodos_capa, numero_bits, numero_clases):

		self.metodo = 'hiperplanos'

		self.numero_bits = numero_bits
		self.nodos_capa = list(nodos_capa)
		self.numero_clases = numero_clases
		self.d = d
		self.weight = []
		self.bias = []
		
		self.x_ph = tf.placeholder(np.float32, shape = (None, d))
		self.y_ph = tf.placeholder(np.float32)

		initializer = tf.contrib.layers.xavier_initializer()

		for i in range(len(nodos_capa)):

			nodos = nodos_capa[i]

			if len(self.weight) == 0:
				shape = [self.d, nodos]
			else:
				shape = [nodos_capa[i-1], nodos]

			self.weight.append(tf.Variable(initializer(shape)))
			self.bias.append(tf.Variable(initializer([1, nodos])))

		##########################################################################

		self.transformation = self.x_ph

		for i in range(len(self.weight)):
			self.transformation = tf.matmul(self.transformation, self.weight[i]) + self.bias[i]
			self.transformation = tf.nn.relu(self.transformation)

		##########################################################################

		if len(nodos_capa) == 0:
			nodos_capa.append(self.d)

		self.weight_hash = tf.Variable(tf.random_normal([nodos_capa[-1], numero_bits]))
		self.hash = tf.matmul(self.transformation, self.weight_hash)
		self.hash = tf.sigmoid(self.hash)

		##########################################################################

		self.weight_class = tf.Variable(initializer([numero_bits, numero_clases]))
		self.bias_class = tf.Variable(initializer([1, numero_clases]))
		self.classification = tf.matmul(self.hash, self.weight_class) + self.bias_class
		#self.cost = tf.nn.softmax_cross_entropy_with_logits(logits = self.classification, labels = self.y_ph)
		#self.cost = tf.reduce_mean(self.cost)

		self.classification = tf.nn.softmax(self.classification)


		#self.classification = tf.nn.softmax(self.classification)

		##########################################################################

		#self.cost = tf.reduce_mean(-self.y_ph * tf.log(self.classification))
		self.cost = tf.reduce_mean((self.y_ph - self.classification)**2)
		self.cost = self.cost + tf.reduce_mean((tf.matmul(tf.transpose(self.weight_hash), self.weight_hash) - tf.eye(numero_bits))**2)

		##########################################################################

		self.agregar_optimizador()

		self.sess = tf.Session()
		self.sess.run(tf.global_variables_initializer())

		##########################################################################



class hashing_RFF(Hashing_Base):

	'''
		d -Dimensión de los datos
		nodos_capa - Lista con nodos, nodos_capa[i] es la cantidad de nodos que tiene la i-ésima capa
		numero_clases - Número de clases en el problema de clasificación
	'''
	def __init__(self, d, nodos_capa, numero_bits, numero_clases, c = 1):

		self.metodo = 'RFF'

		self.c = c
		self.numero_bits = numero_bits
		self.nodos_capa = list(nodos_capa)
		self.numero_clases = numero_clases
		self.d = d
		self.weight = []
		self.bias = []

		self.x_ph = tf.placeholder(np.float32, shape = (None, d))
		self.y_ph = tf.placeholder(np.float32)

		initializer = tf.contrib.layers.xavier_initializer()

		for i in range(len(nodos_capa)):

			nodos = nodos_capa[i]

			if len(self.weight) == 0:
				shape = [self.d, nodos]
			else:
				shape = [nodos_capa[i-1], nodos]

			self.weight.append(tf.Variable(initializer(shape)))
			self.bias.append(tf.Variable(initializer([1, nodos])))
			
		##########################################################################
		
		self.transformation = self.x_ph

		for i in range(len(self.weight)):
			self.transformation = tf.matmul(self.transformation, self.weight[i]) + self.bias[i]
			self.transformation = tf.nn.relu(self.transformation)
	
		##########################################################################

		if len(nodos_capa) == 0:
			nodos_capa.append(self.d)

		self.weight_hash = tf.Variable(tf.random_normal([nodos_capa[-1], numero_bits]))
		self.bias_hash1 = tf.Variable(tf.random_uniform([1, numero_bits], minval = 0, maxval = 2*pi))
		self.bias_hash2 = tf.Variable(tf.random_uniform([1, numero_bits], minval = -1, maxval = 1))

		self.hash = tf.matmul(self.transformation, self.weight_hash) + self.bias_hash1
		self.hash = tf.cos(self.hash) + self.bias_hash2
		self.hash = tf.sigmoid(self.c*self.hash)
		
		##########################################################################

		self.weight_class = tf.Variable(initializer([numero_bits, numero_clases]))
		self.bias_class = tf.Variable(initializer([1, numero_clases]))
		self.classification = tf.matmul(self.hash, self.weight_class) + self.bias_class
		#self.cost = tf.nn.softmax_cross_entropy_with_logits(logits = self.classification, labels = self.y_ph)
		#self.cost = tf.reduce_mean(self.cost)

		self.classification = tf.nn.softmax(self.classification)

		##########################################################################

		#self.cost = tf.reduce_mean(-self.y_ph * tf.log(self.classification))
		self.cost = tf.reduce_mean((self.y_ph - self.classification)**2)
		self.cost = self.cost + tf.reduce_mean((tf.matmul(tf.transpose(self.weight_hash), self.weight_hash) - tf.eye(numero_bits))**2)
		#self.cost = self.cost - 0.2*tf.reduce_mean((self.hash - 0.5)**2)

		self.agregar_optimizador()

		##########################################################################

		self.sess = tf.Session()
		self.sess.run(tf.global_variables_initializer())

		##########################################################################

