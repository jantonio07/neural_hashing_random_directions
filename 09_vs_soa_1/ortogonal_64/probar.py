import numpy as np
from Hashing import Hiperplanos, RFF
from sklearn.preprocessing import normalize
import os


if __name__ == '__main__':

###########################################################

	numero_bits = 48

	print("Cargando datos de entrenamiento")

	x_train = np.load('../datos/entrenamiento.npy')
	y_train = np.load('../datos/etiquetas_entrenamiento.npy')

	print("x_train shape:", x_train.shape)

	print("Cargando datos de prueba")

	x_test = np.load('../datos/prueba.npy')
	y_test = np.load('../datos/etiquetas_prueba.npy')

	print("x_test shape:", x_test.shape)

###########################################################

	media = np.zeros(x_train[0].shape)

	for x in x_train:
		media = media + x

	media /= len(x_train)

	for i in range(len(x_train)):
		x_train[i] = x_train[i] - media

	for i in range(len(x_test)):
		x_test[i] = x_test[i] - media

	x_train = normalize(x_train)
	x_test = normalize(x_test)


###########################################################

	for i in range(10):

		carpeta = "experimento{0}".format(i)
		print(carpeta)

		try:
			os.mkdir(carpeta)
		except:
			pass

		d = x_train.shape[1]

		rh = Hiperplanos(d, numero_bits)
		rff = RFF(d, numero_bits)

		x_rh = rh.hash_code(x_test)
		x_rff = rff.hash_code(x_test)

		np.savetxt(carpeta + '/sigmoide', x_rh, fmt = '%i', delimiter = " ")
		np.savetxt(carpeta + '/rff', x_rff, fmt = '%i', delimiter = " ")
		np.savetxt(carpeta + '/etiquetas', y_test, fmt = '%i', delimiter = "")
