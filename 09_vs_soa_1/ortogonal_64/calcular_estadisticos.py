import os
from sys import argv
import numpy as np
import os


n_resultados = 0

for nombre in os.listdir():
	if nombre[:len('experimento')] == 'experimento':
		n_resultados += 1

p = {}

for i in range(n_resultados):

	print(i)

	carpeta_resultados = 'experimento{0}/'.format(i) + 'resultados/'

	for resultado in os.listdir(carpeta_resultados):

		with open(carpeta_resultados + resultado) as f:
		    lines = f.readlines()


		if not resultado in p:
			p[resultado] = []

		presencia = float(lines[0])
		p[resultado].append(presencia)

archivo = open('resultados', 'w')

####################################################################################################

guardar_lineas = []

archivo.write('recall\n\n')
for (metodo, resultado) in p.items():
	guardar_lineas.append("{0} {1} {2}\n".format(metodo, np.mean(resultado), np.std(resultado)))
	print(metodo, len(resultado))

guardar_lineas.sort()

for linea in guardar_lineas:
	archivo.write(linea)
archivo.write('\n\n')

####################################################################################################
