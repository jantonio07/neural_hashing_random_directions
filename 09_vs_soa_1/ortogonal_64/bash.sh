#!/bin/bash

#1 - Direccion del script
#2 - Nombre para los screen
#3 - Script a ejecutar
#4 - Numero del primer experimento
#5 - Numero del ultimo experimento
#6 - Primer nodo del cluster

for i in `seq $4 $5`;
do

    nodo=$(($i-$4+$6))

    echo $2$i

    sh -c 'screen -dmS '$2$i
    screen -S $2$i -X stuff "ssh c-1-$nodo
"
    screen -S $2$i -X stuff "cd $1
"
    screen -S $2$i -X stuff "source /opt/anaconda351/bin/activate
"
    screen -S $2$i -X stuff "python3 $3 $i
"
    screen -S $2$i -X stuff "ls > $2$i
"
done
