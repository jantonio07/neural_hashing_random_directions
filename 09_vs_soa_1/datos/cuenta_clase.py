import numpy as np


def cuenta(x):

	reg = {}

	for y in x:

		y = y[0]

		if not y in reg:
			reg[y] = 0

		reg[y] += 1

	for (x, y) in reg.items():
		print(x, y)


a = np.load('etiquetas_entrenamiento.npy')
b = np.load('etiquetas_prueba.npy')

a = a.tolist()
b = b.tolist()

print('Conteo en conjunto de entrenamiento:')
cuenta(a)

print('\nConteo en conjunto de prueba')
cuenta(b)
