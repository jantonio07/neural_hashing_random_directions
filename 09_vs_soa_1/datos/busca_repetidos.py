import numpy as np


def repetido(x, i):
	l = []
	for j in range(len(x)):
		if np.linalg.norm(x[i] - x[j]) < 0.001:
			l.append(j)
	if len(l) < 2:
		return False
	else:
		return True


def encuentra(x, X):
	for xi in X:
		if np.linalg.norm(xi - x) < 0.001:
			return True
	return False


a = np.load('entrenamiento.npy')
b = np.load('prueba.npy')

n = 100

fallos = []
for i in range(n):
	print('1', i)
	ind = np.random.randint(0, len(a))
	if repetido(a, ind):
		fallos.append(ind)
print('Repetidos encontrados en muestreo:', fallos)

fallos = []
for i in range(n):
	print('1', i)
	ind = np.random.randint(0, len(b))
	if repetido(b, ind):
		fallos.append(ind)
print('Repetidos encontrados en muestreo:', fallos)

fallos = []
for i in range(n):
	print('1', i)
	ind = np.random.randint(0, len(b))
	if encuentra(a[ind], b):
		fallos.append(ind)
print('Repetidos encontrados en muestreo:', fallos)
