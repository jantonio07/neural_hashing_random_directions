Datos contiene el conjunto de datos cifar procesado con la red VGG16.
Se presenta evidencia de los resultados presentados en la subseccion
4.4.4. Comparacion con el estado del arte. Como la carpeta original
pesa 11.1 gb solo se pone un experimento para cada metodo, aun asi,
es posible hacer mas con los codigos.

Para hacer un nuevo experimento se recomiendo modificar el codigo
dona_prueba.py, de lo contrario, se crean dos grafos durante la eje-
cucion y algunas versiones de tensorflow se ponen lentas con el segundo.

dona_prueba.py recibe como argumento el numero del experimento. Por
ejemplo, si se quiere hacer un experimento con id 1, se ejecuta

python3 dona_prueba.py 1

al terminar la ejecucion debe haber una carpeta llamada experimento1, a
continuacion se lista el contenido.

carpetas:
model_RFF.ckpt	
model_hiperplanos.ckpt

archivos:
etiquetas_prueba
rff_hash_prueba
rff_redondeo_prueba
sigmoide_hash_prueba
sigmoide_redondeo_prueba

Las carpetas contienen los pesos de la red neuronal al finalizar el
entrenamiento. En los x_hash_prueba se almacenan las salidas de la penul-
tima capa y en los x_redondeo_prueba los codigos binarios que se obtienen
al redondear las entradas de x_hash_prueba.

Para evaluar los modelos del experimento x, se debe correr

python3 calcular_resultados.py x

Al terminar crea una carpeta que se llama "resultados" y diferentes archivos
de texto, cada archivo contiene un resultado, si se llama rff_hash_prueba
contien el mAP que se obtiene al tomar los datos de rff_hash_prueba con las
etiquetas del archivo etiquetas_prueba. mAP se calcula con el binario knn_map,
binario generado con el codigo de la carpeta 01_knn. Los parametros para el
binario se pasan de manera automatica en el script.

Despues de hacer varios experimentos se ejecuta

python3 calcular_estadisticos.py

y crea un archivo de texto llamado "resultados" que contiene el promedio de
los mAP de todos los experimentos existentes.

Los otros scripts en bash fueron usados para utilizar El Insurgente.
