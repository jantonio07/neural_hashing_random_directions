import tensorflow as tf
from math import pi
import numpy as np


class Hashing():

	def hash_code(self, X):

		X = np.atleast_2d(X)

		feed_dict = {self.x_ph : X}
		return self.sess.run(self.hashing, feed_dict = feed_dict)

	def asignar_pesos(self, X):

		self.weight = X


class Redondeo(Hashing):

	def __init__(self, d):

		self.x_ph = tf.placeholder(tf.float32, shape = (None, d))

		self.hashing = self.x_ph - 0.5
		self.hashing = (tf.sign(self.hashing) + 1)/2

		self.sess = tf.Session()
		self.sess.run(tf.global_variables_initializer())


class Hiperplanos(Hashing):

	def __init__(self, d, dim_hash):

		self.x_ph = tf.placeholder(tf.float32, shape = (None, d))	

		self.weight = tf.random_normal([d, dim_hash])
		self.hashing = tf.matmul(self.x_ph, self.weight)
		self.hashing = (tf.sign(self.hashing) + 1)/2

		self.sess = tf.Session()
		self.sess.run(tf.global_variables_initializer())


class RFF(Hashing):

	def __init__(self, d, dim_hash):

		self.x_ph = tf.placeholder(tf.float32, shape = (None, d))	
		self.weight = tf.random_normal([d, dim_hash])

		self.bias = []
		self.bias.append(tf.random_uniform([1, dim_hash], minval = 0, maxval = 2*pi))
		self.bias.append(tf.random_uniform([1, dim_hash], minval = -1, maxval = 1))

		self.hashing = tf.cos(tf.matmul(self.x_ph, self.weight) + self.bias[0]) + self.bias[1]
		self.hashing = (tf.sign(self.hashing) + 1)/2

		self.sess = tf.Session()
		self.sess.run(tf.global_variables_initializer())


class Autoencoder():

	def __init__(self, d, n):

		self.metodo = 'autoencoder'
		self.n = n

		self.weight = []
		self.weight.append(tf.Variable(tf.random_normal([d, n])))
		
		self.x_ph = tf.placeholder(np.float32, shape = (None, d))
		self.y_ph = tf.placeholder(np.float32, shape = (None, d))

		self.encoder = tf.matmul(self.x_ph, self.weight[0])
		self.decoder = tf.matmul(self.encoder, tf.transpose(self.weight[0]))
		self.costo = tf.reduce_mean((self.y_ph - self.decoder)**2)
		self.costo = self.costo + tf.reduce_mean((tf.matmul(tf.transpose(self.weight[0]), self.weight[0]) - tf.eye(n))**2)

		self.optimizer = tf.train.AdamOptimizer().minimize(self.costo)

		self.sess = tf.Session()
		self.sess.run(tf.global_variables_initializer())

	def entrenar(self, X, epochs, batch_size, hot_encoding = True, k_ultimos = 10, tolerancia = 1e-12):

			errores_pasados = []

			##########################################################################

			for epoch in range(epochs):

				print("Epoch", epoch+1, "de", epochs, end = ' ({0}).'.format(self.metodo))
				error_sum = 0

				t_epochs = len(X)//batch_size

				if len(X)%batch_size != 0:
					t_epochs += 1

				for i_batch in range(t_epochs):
					start_ind = i_batch*batch_size
					end_ind = min(len(X), (i_batch+1)*batch_size)

					x_epoch = X[start_ind:end_ind]

					feed_dict = {self.x_ph : x_epoch,
	                			 self.y_ph : x_epoch}

					_, loss_val = self.sess.run([self.optimizer, self.costo], feed_dict = feed_dict)
					error_sum += loss_val

				print(" Loss function:", error_sum)

				##########################################################################

				errores_pasados.append(error_sum)
				
				if len(errores_pasados) == k_ultimos + 1:
					errores_pasados = errores_pasados[1:]

				if (len(errores_pasados) == k_ultimos) and (np.std(errores_pasados) < tolerancia):
					print('Criterio de convergencia cumplido')
					return

				##########################################################################


			##########################################################################

	def encode(self, X):

		X = np.atleast_2d(X)

		feed_dict = {self.x_ph : X}
		return self.sess.run(self.encoder, feed_dict = feed_dict)

	def decode(self, X):

		X = np.atleast_2d(X)

		feed_dict = {self.x_ph : X}
		return self.sess.run(self.decoder, feed_dict = feed_dict)

	def pesos(self):

		return self.sess.run(self.weight[0])
