import os
import sys
import numpy as np

###########################################################

carpeta = sys.argv[1]

if carpeta[-1] != '/':
	carpeta += '/'

###########################################################

nombre_archivo = []

for x in os.listdir(carpeta):
	if x[:len('sigmoide')] == 'sigmoide' or x[:len('rff')] == 'rff':
		nombre_archivo.append(x)

n_codigos = len(nombre_archivo)

for i in range(n_codigos):

	try:
		os.mkdir(carpeta + 'resultados')
	except:
		pass

	tipo = nombre_archivo[i].split('_')[-1]
	etiquetas = np.loadtxt(carpeta + 'etiquetas_' + tipo)

	clases = len(set(etiquetas))
	n_clase = len(etiquetas)//clases

	print("clases:", clases)
	print("n por clase:", n_clase)

	arg0 = carpeta + nombre_archivo[i]
	arg1 = carpeta + 'etiquetas_' + tipo
	arg2 = 10000
	arg3 = carpeta + 'resultados/' + nombre_archivo[i]

	comando = './knn_map {0} {1} {2} {3}'.format(arg0, arg1, arg2, arg3)

	print(comando)
	os.system(comando)

