from math import pi
import numpy as np
import os
import tensorflow as tf
from modelos import hashing_hiperplanos, hashing_RFF
from Hashing import Redondeo
import sys
import math
from sklearn.preprocessing import normalize, scale


if __name__ == '__main__':

#############################################################################################

	k = 10
	espacio = 4096
	numero_bits = 64

	epochs = 500
	batch_s = 512

	layers = [2072]

#############################################################################################

	try:
		carpeta = 'experimento' + str(sys.argv[1]) + '/'
		os.mkdir(carpeta)
	except:
		pass

#############################################################################################

	print("Cargando datos de entrenamiento")

	X_train = np.load('../datos/entrenamiento.npy')
	Y_train = np.load('../datos/etiquetas_entrenamiento.npy')

	print("x_train shape:", X_train.shape)

	print("Cargando datos de prueba")

	X_test = np.load('../datos/prueba.npy')
	Y_test = np.load('../datos/etiquetas_prueba.npy')

	print("x_test shape:", X_test.shape)

#############################################################################################

	media = np.zeros(X_train[0].shape)

	for x in X_train:
		media = media + x

	media /= len(X_train)

	for i in range(len(X_train)):
		X_train[i] = X_train[i] - media

	for i in range(len(X_test)):
		X_test[i] = X_test[i] - media

	X_train = normalize(X_train)
	X_test = normalize(X_test)


#############################################################################################

	redondeo = Redondeo(numero_bits)

#############################################################################################

	try:
		os.mkdir(carpeta + "model_hiperplanos.ckpt")
	except:
		pass

	sig_hash = hashing_hiperplanos(espacio, layers, numero_bits, k)
	sig_hash.entrenar(X_train, Y_train, epochs, batch_s, hot_encoding = True, carpeta = carpeta + "model_hiperplanos.ckpt/")

	sig_hash_ = sig_hash.aplicar_hashing(X_test)
	sig_red = redondeo.hash_code(sig_hash_)

	np.savetxt(carpeta + '/sigmoide_hash_prueba', sig_hash_)
	np.savetxt(carpeta + '/sigmoide_redondeo_prueba', sig_red, fmt = '%i', delimiter = " ")

##########################################################################################################

	''' 
	try:
		os.mkdir(carpeta + "model_RFF.ckpt")
	except:
		pass

	rff_hash = hashing_RFF(espacio, layers, numero_bits, k)
	rff_hash.entrenar(X_train, Y_train, epochs, batch_s, hot_encoding = True, carpeta = carpeta + "model_RFF.ckpt/")

	rff_hash_ = rff_hash.aplicar_hashing(X_test)
	rff_red = redondeo.hash_code(rff_hash_)

	np.savetxt(carpeta + '/rff_hash_prueba', rff_hash_)
	np.savetxt(carpeta + '/rff_redondeo_prueba', rff_red, fmt = '%i', delimiter = " ")
	'''

##########################################################################################################

	np.savetxt(carpeta + '/etiquetas_prueba', Y_test, fmt = '%i', delimiter = "")
