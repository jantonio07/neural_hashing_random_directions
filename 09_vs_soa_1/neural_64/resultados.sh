#!/bin/bash

# 1 - Direccion
# 2 - Primer experimento
# 3 - Ultimo experimento
# 4 - Nombre de ventana
# 5 - Nodo inicial

for i in `seq $2 $3`;
do

    nodo=$(($i-$2+$5))

    sh -c 'screen -dmS '$4$i
    screen -S $4$i -X stuff "ssh c-1-$nodo
"
    screen -S $4$i -X stuff "cd $1
"
    screen -S $4$i -X stuff "source /opt/anaconda351/bin/activate
"
    screen -S $4$i -X stuff "python3 calcular_resultados.py experimento$i
"
    screen -S $4$i -X stuff "ls > out$i
"
done
