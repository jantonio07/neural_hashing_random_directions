Se presenta evidencia de los experimentos realizados en el trabajo "Hashing
neuronal supervisado basado en hashing localmente sensible aplicado al pro-
blema de information retrieval".

Algunas carpetas tienen un README con mas detalles. Los programas se corrieron
en el cluster El Insurgente del CIMAT, adjunto vienen los codigos que se utili-
zaron para distribuir las ejecuciones en distintos nodos. requirements.txt
tiene una lista con las librerias instaladas en el virtual environment que se
utilizo para correr los codigos.


Caracteristicas de cada nodo
	Hardware
		CPUs: 24 x 2.60 GHz
		Memory (RAM): 31.39 GB
	Software
		OS: Linux 4.8.0-32-generic (x86_64)


01_knn: Codigo en C++ para generar binario que calcule mAP usando la libreria dlib.
En CMakeLists.txt vienen instrucciones para compilar con cmake. En la carpeta
esta la libreria, solo hay que compilar.

02_clasificador_softmax: Jupyter-Notebook en Python para explorar la geometria en
las salidas de la penultima capa en una red neurunal/softmax classifier. La
implementacion esta en keras y se usa tensorflow de backend.

04_vgg_tf: Implementacion de VGG16 en Python usando tensorflow. El codigo se tomo
de la pagina especificada en vgg.py y se modifico para procesar bases de datos.
Despues de procesar, los resultados se almacenan manualmente (copiar y pegar) en
09_vs_soa_1/datos.

09_vs_soa_1: Cada experimento pesa aproximadamente 200 mb, por eso, a pesar de haber
realizado 10 experimentos para cada modelo solo se presenta el numero 0. Adjunto a cada
carpeta viene un script llamado modelos.py, que contiene las clases para binarizar
los datos. El script dona_prueba.py es el script principal y el que se debe ejecutar
para hacer un experimento.  


