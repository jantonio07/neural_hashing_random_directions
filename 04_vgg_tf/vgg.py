########################################################################################
# Davi Frossard, 2016                                                                  #
# VGG16 implementation in TensorFlow                                                   #
# Details:                                                                             #
# http://www.cs.toronto.edu/~frossard/post/vgg16/                                      #
#                                                                                      #
# Model from https://gist.github.com/ksimonyan/211839e770f7b538e2d8#file-readme-md     #
# Weights from Caffe converted using https://github.com/ethereon/caffe-tensorflow      #
########################################################################################

import tensorflow as tf
import numpy as np
from scipy.misc import imread, imresize
from imagenet_classes import class_names
from sklearn.preprocessing import OneHotEncoder


class vgg16:
    ''' 
        Carga el modelo VGG a partir del placeholder imgs
        En caso de recibir weights y sess, carga los pesos weights en la sesion sess
    '''
    def __init__(self, load_weights = True, path_weights = '../vgg16_weights.npz'):

        self.sess = tf.Session() #Se crea una sesion para el objeto
        self.imgs = tf.placeholder(tf.float32, [None, 224, 224, 3]) #Placeholder para ingresar las imagenes

        self.convlayers() #Creal el flujo de capas convolucionales, la ultima capa es pool5
        self.fc_layers() #Crea capas densas a partir de pool5, la ultima capa es fc3l
        self.probs = tf.nn.softmax(self.fc3l) #Softmax para distribucion de probabilidad

        if load_weights:
            print('\nCargando pesos\n')
            self.load_weights(path_weights)

    ''' 
	Crea capas convolucionales a partir del placeholder 'imgs'

    '''
    def convlayers(self):
        self.parameters = []

        # zero-mean input
        with tf.name_scope('preprocess') as scope:
            mean = tf.constant([123.68, 116.779, 103.939], dtype=tf.float32, shape=[1, 1, 1, 3], name='img_mean')
            images = self.imgs-mean

        # conv1_1
        with tf.name_scope('conv1_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 3, 64], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(images, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[64], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv1_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv1_2
        with tf.name_scope('conv1_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 64, 64], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv1_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[64], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv1_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool1
        self.pool1 = tf.nn.max_pool(self.conv1_2,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool1')

        # conv2_1
        with tf.name_scope('conv2_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 64, 128], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.pool1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[128], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv2_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv2_2
        with tf.name_scope('conv2_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 128, 128], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv2_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[128], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv2_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool2
        self.pool2 = tf.nn.max_pool(self.conv2_2,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool2')

        # conv3_1
        with tf.name_scope('conv3_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 128, 256], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.pool2, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv3_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv3_2
        with tf.name_scope('conv3_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 256], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv3_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv3_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv3_3
        with tf.name_scope('conv3_3') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 256], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv3_2, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv3_3 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool3
        self.pool3 = tf.nn.max_pool(self.conv3_3,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool3')

        # conv4_1
        with tf.name_scope('conv4_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.pool3, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv4_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv4_2
        with tf.name_scope('conv4_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv4_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv4_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv4_3
        with tf.name_scope('conv4_3') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv4_2, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv4_3 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool4
        self.pool4 = tf.nn.max_pool(self.conv4_3,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool4')

        # conv5_1
        with tf.name_scope('conv5_1') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.pool4, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv5_1 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv5_2
        with tf.name_scope('conv5_2') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv5_1, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv5_2 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # conv5_3
        with tf.name_scope('conv5_3') as scope:
            kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                                     stddev=1e-1), name='weights')
            conv = tf.nn.conv2d(self.conv5_2, kernel, [1, 1, 1, 1], padding='SAME')
            biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                                 trainable=True, name='biases')
            out = tf.nn.bias_add(conv, biases)
            self.conv5_3 = tf.nn.relu(out, name=scope)
            self.parameters += [kernel, biases]

        # pool5
        self.pool5 = tf.nn.max_pool(self.conv5_3,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME',
                               name='pool4')

    def fc_layers(self):
        # fc1
        with tf.name_scope('fc1') as scope:
            shape = int(np.prod(self.pool5.get_shape()[1:]))
            fc1w = tf.Variable(tf.truncated_normal([shape, 4096],
                                                         dtype=tf.float32,
                                                         stddev=1e-1), name='weights')
            fc1b = tf.Variable(tf.constant(1.0, shape=[4096], dtype=tf.float32),
                                 trainable=True, name='biases')
            pool5_flat = tf.reshape(self.pool5, [-1, shape])
            fc1l = tf.nn.bias_add(tf.matmul(pool5_flat, fc1w), fc1b)
            self.fc1 = tf.nn.relu(fc1l)
            self.parameters += [fc1w, fc1b]

        # fc2
        with tf.name_scope('fc2') as scope:
            fc2w = tf.Variable(tf.truncated_normal([4096, 4096],
                                                         dtype=tf.float32,
                                                         stddev=1e-1), name='weights')
            fc2b = tf.Variable(tf.constant(1.0, shape=[4096], dtype=tf.float32),
                                 trainable=True, name='biases')
            fc2l = tf.nn.bias_add(tf.matmul(self.fc1, fc2w), fc2b)
            self.fc2 = tf.nn.relu(fc2l)
            self.parameters += [fc2w, fc2b]

        # fc3
        with tf.name_scope('fc3') as scope:
            fc3w = tf.Variable(tf.truncated_normal([4096, 1000],
                                                         dtype=tf.float32,
                                                         stddev=1e-1), name='weights')
            fc3b = tf.Variable(tf.constant(1.0, shape=[1000], dtype=tf.float32),
                                 trainable=True, name='biases')
            self.fc3l = tf.nn.bias_add(tf.matmul(self.fc2, fc3w), fc3b)
            self.parameters += [fc3w, fc3b]


    def transformar(self, X):

        return self.sess.run(self.fc2, feed_dict = {self.imgs : X})

    def load_weights(self, weight_file):
        weights = np.load(weight_file)
        keys = sorted(weights.keys())
        for i, k in enumerate(keys):
            print(i, k, np.shape(weights[k]))
            self.sess.run(self.parameters[i].assign(weights[k]))


class Base(vgg16):

    def __init__(self):

        #Capa previa a clasificarion self.fc2
        vgg16.__init__(self)


    def entrenar(self, X, Y, epochs, batch_size, hot_encoding = True, k_ultimos = 10, tolerancia = 1e-6, carpeta = ""):

        if hot_encoding:

            Y = np.array(Y)
            Y = Y.reshape(len(Y), 1)
            onehot_encoder = OneHotEncoder(sparse=False)
            Y = onehot_encoder.fit_transform(Y)

        errores_pasados = []

        ##########################################################################

        valor_cp = -1
        saver = tf.train.Saver()
        carpeta_modelo = carpeta + "model_{0}.ckpt".format(self.metodo)

        for epoch in range(epochs):

            print("Epoch", epoch+1, "de", epochs, end = ' ({0}).'.format(self.metodo))
            error_sum = 0

            t_epochs = len(X)//batch_size

            if len(X)%batch_size != 0:
                t_epochs += 1

            for i_batch in range(t_epochs):

                print("   ", epoch, "-", i_batch, "-", t_epochs)

                start_ind = i_batch*batch_size
                end_ind = min(len(X), (i_batch+1)*batch_size)

                x_epoch = X[start_ind:end_ind]
                y_epoch = Y[start_ind:end_ind]

                feed_dict = {self.imgs : x_epoch,
                             self.y_ph : y_epoch}

                _, loss_val = self.sess.run([self.optimizer, self.cost], feed_dict = feed_dict)
                error_sum += loss_val

                print("       ", error_sum)

            print(" Loss function:", error_sum, end = '. ')

            ##########################################################################

            errores_pasados.append(error_sum)

            if len(errores_pasados) == k_ultimos + 1:
                errores_pasados = errores_pasados[1:]

            print('std =', np.std(errores_pasados))

            if (len(errores_pasados) == k_ultimos) and (np.std(errores_pasados) < tolerancia):
                print('Criterio de convergencia cumplido')
                return

            if valor_cp == -1 or error_sum < valor_cp:

                valor_cp = error_sum/2
                print("Intentando guardar en", carpeta_modelo)
                save_path = saver.save(self.sess, carpeta_modelo)
                print("Modelo guardado en: {0}".format(save_path))

                registro = open(carpeta + 'funcion_error', 'w')
                registro.write('{0}\n'.format(error_sum))
                registro.close()

        ##########################################################################

        registro = open(carpeta + 'funcion_error')
        for line in registro:
            error_guardado = float(line)

        if error_sum < error_guardado:
            print("Intentando guardar en", carpeta_modelo)
            save_path = saver.save(self.sess, carpeta_modelo)
            print("Modelo guardado en: {0}".format(save_path))

            registro = open(carpeta + 'funcion_error', 'w')
            registro.write('{0}\n'.format(error_sum))
            registro.close()
        else:
            print("cargando modelo")
            saver.restore(self.sess, carpeta_modelo)

        ##########################################################################


    def agregar_optimizador(self):

        self.optimizer = tf.train.AdamOptimizer().minimize(self.cost)


    ''' 
        X - Conjunto de datos a los que se quiere aplicar transformacion (penultima capa del modelo neuronal)
    '''
    def transformacion(self, X):

        X = np.atleast_2d(X)

        feed_dict = {self.imgs : X}
        return self.sess.run(self.fc2, feed_dict = feed_dict)


    def aplicar_hashing(self, X):

        X = np.atleast_2d(X)

        feed_dict = {self.imgs : X}
        return self.sess.run(self.hash, feed_dict = feed_dict)


    def clasificar(self, img):

        prob = self.sess.run(self.probs, feed_dict={self.imgs: [img]})[0]
        preds = (np.argsort(prob)[::-1])[0:5]

        for p in preds:
            print(class_names[p], prob[p])

        return preds


class Hiperplanos_Aleatorios(Base):

    def __init__(self, numero_bits, numero_clases):

        #Capa previa a clasificarion self.fc2, tiene 4096 nodos
        Base.__init__(self)

        self.metodo = 'hiperplanos_aleatorios'

        ##########################################################################

        initializer = tf.contrib.layers.xavier_initializer()
        self.y_ph = tf.placeholder(tf.float32, shape = [None, numero_clases])

        self.weight_hash = tf.Variable(tf.random_normal([4096, numero_bits]))

        self.hash = tf.matmul(self.fc2, self.weight_hash)
        self.hash = tf.sigmoid(self.hash)

        ##########################################################################

        self.weight_class = tf.Variable(initializer([numero_bits, numero_clases]))
        self.bias_class = tf.Variable(initializer([1, numero_clases]))

        self.classification = tf.matmul(self.hash, self.weight_class) + self.bias_class
        self.classification = tf.nn.softmax(self.classification)

        ##########################################################################

        self.cost = tf.reduce_mean((self.y_ph - self.classification)**2)

        ##########################################################################

        self.agregar_optimizador()

        adam_vars = [var for var in tf.global_variables()
                     if 'Adam' in var.name or
                     'beta1_power' in var.name or
                     'beta2_power' in var.name]

        init_new_vars_op = tf.initialize_variables([self.weight_hash, self.weight_class, self.bias_class] + adam_vars)
        self.sess.run(init_new_vars_op)

        #self.sess.run(self.weight_hash.initializer)
        #self.weight_class = tf.Variable(initializer([numero_bits, numero_clases]))
        #self.bias_class = tf.Variable(initializer([1, numero_clases]))

        #self.sess.run(tf.variables_initializer(self.optimizer.variables()))

        #init = tf.initialize_all_variables()
        #self.sess.run(init)


if __name__ == '__main__':

    print('Cargando conjunto de entrenamiento...')
    x_train = np.load('x_train_res.npy')
    y_train = np.load('y_train.npy')

    print('Cargando conjunto de prueba...')
    x_test = np.load('x_test_res.npy')
    y_test = np.load('y_test.npy')

    print('listo!\n')

    print("x_train", x_train.shape)
    print("y_train", y_train.shape)

    print("x_test", x_test.shape)
    print("y_test", y_test.shape)

    hash_rh = Hiperplanos_Aleatorios(48, 10)
    print('se creo...')

    #def entrenar(self, X, Y, epochs, batch_size, hot_encoding = True, k_ultimos = 10, tolerancia = 1e-6, carpeta = ""):
    hash_rh.entrenar(x_train, y_train, 10, 128)
