import numpy as np
import sys
import os

try:
  os.mkdir('datos')
except:
  pass

entrenamiento = []
prueba = []

for i in range(100):

  entrenamiento.extend(np.load('entrenamiento/batch_{0}.npy'.format(i)))
  prueba.extend(np.load('prueba/batch_{0}.npy'.format(i)))

print("Entrenamiento:", len(entrenamiento))
print("Prueba:", len(prueba))

np.save('datos/entrenamiento', entrenamiento)
np.save('datos/prueba', prueba)
