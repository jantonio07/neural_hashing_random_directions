Los pesos de la VGG16 estan en vgg16_weights.npz y la carpeta
procesar_cifar contiene los scripts para procesar cifar usando
tensorflow.

Para procesar cifar se ejecuta procesar_cifar/vgg.py, durante la
ejecucion del programa se notifica el progreso, pues procesa la
base por batches. Despues de procesar cada batch lo almacena en una
carpeta, al terminar la ejecucion se debe ejecutar juntar_batches.py
para crear un solo archivo con los datos procesados, posteriormente
se hace lo deseado con la carpeta datos.
