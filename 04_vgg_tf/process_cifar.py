from scipy.misc import imresize
import numpy as np
import sys


def resize_images(imagenes, msg):

    ans = []

    for i in range(imagenes.shape[0]):

        print(msg, i)
	#nimg = imresize(x_train[i], (227, 227))
	#nimg = nimg - mean(nimg)
        ans.append(imresize(x_train[i], (224, 224)))

    return np.array(ans)


if __name__ == '__main__':

    x_train = np.load('x_train.npy')
    y_train = np.load('y_train.npy')

    x_test = np.load('x_test.npy')
    y_test = np.load('y_test.npy')

    x_train_ = resize_images(x_train, 'entrenamiento')
    x_test_ = resize_images(x_test, 'prueba')

    np.save('x_train_res', x_train_)
    np.save('x_test_res', x_test_)

